require('dotenv').config();
const port = process.env.PORT || 80;
const expressBearerToken = require('express-bearer-token');
const expresshbs = require('express-handlebars');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const express = require('express');
const morgan = require('morgan');
const csurf = require('csurf');
const hbs = expresshbs.create({
	partialsDir: [
		'views/partials/'
	],
	helpers: {
		eq: function(a, b, opts) {
			if (a == b)
				return opts.fn(this);
			else
				return opts.inverse(this);
		},
		ueq: function(a, b, opts) {
			if (a != b)
				return opts.fn(this);
			else
				return opts.inverse(this);
		}
	}
});
const accounts = require('./src/accounts');
const playlists = require('./src/playlists');
const songs = require('./src/songs');
const render = require('./src/render');
const fetchMessage = require('./src/messages').createMessage;
const helmet = require('helmet');
const app = express();

app.use(helmet());
app.use(morgan('combined'));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(expressBearerToken());
app.use(csurf({ cookie: true }));
app.use(function (err, req, res, next) {
	if (err.code !== 'EBADCSRFTOKEN') {
		return next(err);
	}
	res.clearCookie("Authorization");
	res.redirect('/login?message=' + "Form was compromised.&type=error");
});
app.use('/assets', express.static('assets'));
app.use('/lib', express.static('lib'));
app.use('/static', express.static('./src/static'));
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');
app.get('/', function(_, res) {
	render.landing(res);
});
app.get('/login', function(req, res) {
	res.clearCookie("Authorization");
	render.login(req, res, fetchMessage(req));
});
app.get('/signup', function(req, res) {
	render.signup(req, res, fetchMessage(req));
});
app.get('/about', function(req, res) {
	render.about(res, req.cookies["Authorization"] || undefined);
});
app.get('/contact', function(req, res) {
	render.contact(res, req.cookies["Authorization"] || undefined);
});
app.use('/accounts', accounts);
app.use('/playlists', playlists);
app.use('/songs', songs);
app.listen(port, () => console.log(`Server listening on port ${port}!`));