const request = require('request-promise');
const environment = process.env.PRODUCTION == "FALSE" ? require('../../environments/development.json') : require('../../environments/production.json');
const errors = require('../errors');

function makeRequest(link, method, json, token = undefined) {
	const options = {
		method: method,
		json: json,
		url: environment.api + link,
		headers: token ? {
			'Authorization': token
		} : {}
	};

	return new Promise((resolve, reject) => {
		request(options)
		.then(function(response) {
			resolve(response);
		})
		.catch(function(err) {
			if (err.statusCode == 401) {
				reject(new errors.Unauthenticated());
			} else {
				reject(err);
			}
		});
	});

}

module.exports = makeRequest;