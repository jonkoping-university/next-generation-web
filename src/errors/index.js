class Unauthenticated extends Error {
	constructor(message = "You need to be logged in.") {
		super(message);
	}
}

class Unauthorized extends Error {
	constructor(message = "Access denied.") {
		super(message);
	}
}

class InvalidRequest extends Error {
	constructor(message = "Invalid request.") {
		super(message);
	}
}

module.exports = {
	Unauthenticated: Unauthenticated,
	Unauthorized: Unauthorized,
	InvalidRequest: InvalidRequest
}