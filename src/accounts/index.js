const express = require('express');
const router = express.Router();
const verify = require('../token').verify;
const requester = require('../requester');
const errors = require('../errors');
const renderAccounts = require('../render').accounts;
const redirect = require('../redirect');

router.post('/authenticate', function(req, res) {
	requester('/accounts/authenticate', 'POST', req.body)
	.then(function(body) {
		if (!body.data || !body.data.token) {
			throw new errors.InvalidRequest(body.message || "Invalid credentials.");
		}
		res.cookie('Authorization', 'Bearer ' + body.data.token, { maxAge: 60 * 60 * 1000, path: '/', httpOnly: true, secure: false, sameSite: "strict" });
		res.redirect('/playlists');
	}).catch(function(err) {
		if (err instanceof errors.Unauthenticated) {
			err.message = "Invalid credentials.";
		}
		redirect(res, 'login', err);
	});
});

router.post('/signup', function(req, res) {
	requester('/accounts', 'POST', req.body)
	.then(function(body) {
		if (body.type != 'success') {
			throw new errors.InvalidRequest(body.message || 'Invalid information.');
		}
		res.redirect('/login?message=' + "Your account was created." + "&type=success");
	}).catch(function(err) {
		redirect(res, 'signup', err);
	});
});

router.use(function(req, res, next) {
	verify(req, res, next);
});

router.get('/', function(req, res) {
	requester('/accounts/' + (req.query.query ? "?query=" + req.query.query : ""), 'GET', true, req.cookies["Authorization"])
	.then(function(body) {
		if (!body.data || !body.data.accounts) {
			throw new errors.InvalidRequest(body.message || 'Invalid information.');
		}
		renderAccounts(res, body.data.accounts, req.query.query, req.cookies["Authorization"]);
	}).catch(function(err) {
		redirect(res, 'login', err);
	});
});

module.exports = router;