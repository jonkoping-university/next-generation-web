const requester = require('../requester');
const errors = require('../errors');

function getAccount(id, token) {
	return new Promise((resolve, reject) => {
		requester('/accounts/' + id, 'GET', true, token)
		.then(function(body) {
			if (!body.data || !body.data.account) {
				throw new errors.Unauthenticated();
			} else {
				resolve(body.data.account);
			}
		}).catch(function(err) {
			reject(err);
		});
	});
}

module.exports = getAccount