function redirectWithError(res, page, err) {
	if (err.error && err.error.message && err.error.type) {
		res.redirect('/' + page + '?message=' + err.error.message + "&type=" + err.error.type);
	} else {
		res.redirect('/' + page + '?message=' + err.message + "&type=error");
	}
}

module.exports = redirectWithError