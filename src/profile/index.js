global.atob = require("atob");

function fetchProfileFromToken(token) {
	var base64Url = token.split('.')[1];
	var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
	var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
		return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
	}).join(''));

	jsonPayload = JSON.parse(jsonPayload);
	var initials = jsonPayload.fullname.match(/\b\w/g) || [];
	jsonPayload.initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
	return jsonPayload;
}

module.exports = fetchProfileFromToken;