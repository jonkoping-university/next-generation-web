function verify(req, res, next) {
	if (!req.cookies || !req.cookies["Authorization"]) {
		res.redirect('/login');
		return 84;
	}
	next();
}

module.exports = {
	verify
}