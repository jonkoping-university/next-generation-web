function createMessage(req) {
	if (req.query.message != undefined && req.query.type != undefined) {
		return {content: req.query.message, type: req.query.type};
	}
	return undefined;
}

module.exports = {
	createMessage: createMessage
}