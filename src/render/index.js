const fetchProfile = require('../profile');
const timeago = require('timeago.js');

function renderPage(res, name, options) {
	res.render(name, options);
}

function buildOptions(name, token = undefined, title = name.charAt(0).toUpperCase() + name.slice(1)) {
	let options = {
		active: {},
		styles: [name + ".css"],
		profile: token ? fetchProfile(token.replace('Bearer ', "")) : undefined,
		title: title
	};

	options.active[name] = true;
	return options;
}

function renderAccounts(res, accounts, query, token, message = undefined) {
	let options = buildOptions("community", token);
	options.accounts = accounts;
	options.query = query;
	options.message = message;
	renderPage(res, "community", options);
}

function renderPlaylists(req, res, playlists, token, account = undefined, message = undefined) {
	let options = buildOptions("playlists", token);
	options.active = account ? { community: true } : { playlists: true },
	options.playlists = playlists;
	options.account = account;
	options.message = message;
	options.csrfToken = req.csrfToken();
	renderPage(res, "playlists", options);
}

function renderPlaylist(req, res, playlist, songs, token, message = undefined) {
	let options = buildOptions("playlist", token, "Playlist - " + playlist.name);
	const account = fetchProfile(token.replace('Bearer ', ""));

	playlist.modified = timeago.format(playlist.modified * 1000);
	options.active = playlist.owner_id == account.id ? { playlists: true } : { community: true },
	options.playlist = playlist;
	options.songs = songs;
	options.profile = account;
	options.message = message;
	options.csrfToken = req.csrfToken();
	renderPage(res, "playlist", options);
}

function renderSongs(req, res, playlist, songs, query, token, message = undefined) {
	let options = buildOptions("songs", token, "Add Song");
	const account = fetchProfile(token.replace('Bearer ', ""));

	options.active = { playlists: true },
	options.playlist = playlist;
	options.songs = songs;
	options.query = query;
	options.profile = account;
	options.message = message;
	options.csrfToken = req.csrfToken();
	renderPage(res, "songs", options);
}

function renderAbout(res, token) {
	const options = buildOptions('about', token);

	renderPage(res, 'about', options);
}

function renderContact(res, token) {
	const options = buildOptions('contact', token);

	renderPage(res, 'contact', options);
}

function renderSignup(req, res, message = undefined) {
	const options = buildOptions('signup');

	options.message = message;
	options.csrfToken = req.csrfToken();
	renderPage(res, 'signup', options);
}

function renderLogin(req, res, message = undefined) {
	const options = buildOptions('login');

	options.message = message;
	options.csrfToken = req.csrfToken();
	renderPage(res, 'login', options);
}

function renderLanding(res) {
	const options = buildOptions('landing', undefined, "Playlist Manager");

	renderPage(res, 'landing', options);
}

module.exports = {
	page: renderPage,
	playlists: renderPlaylists,
	playlist: renderPlaylist,
	accounts: renderAccounts,
	contact: renderContact,
	about: renderAbout,
	signup: renderSignup,
	login: renderLogin,
	landing: renderLanding,
	songs: renderSongs
}