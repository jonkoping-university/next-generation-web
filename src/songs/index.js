const express = require('express');
const router = express.Router();
const verify = require('../token').verify;
const requester = require('../requester');
const errors = require('../errors');
const redirect = require('../redirect');

router.use(function(req, res, next) {
	verify(req, res, next);
});

router.post('/', function(req, res) {
	requester('/songs/', 'POST', { name: req.body.name, playlist: req.body.playlist, uri: req.body.uri }, req.cookies["Authorization"])
	.then(function(body) {
		if (body.type != "success") {
			throw new errors.InvalidRequest(body.message || "Couldn't add song to playlist.");
		}
		res.redirect('/playlists/' + req.body.playlist + "?message=Song added.&type=success");
	}).catch(function(err) {
		redirect(res, 'playlists/' + Number(req.body.playlist), err);
	});
});

router.post('/delete', function(req, res) {
	requester('/songs/' + Number(req.body.song), 'DELETE', req.body, req.cookies["Authorization"])
	.then(function(body) {
		if (body.type != "success") {
			throw new errors.InvalidRequest(body.message || "Couldn't delete song.");
		}
		res.redirect('/playlists/' + req.body.playlist + "?message=Song deleted.&type=success");
	}).catch(function(err) {
		redirect(res, 'playlists/' + Number(req.body.playlist), err);
	});
});

module.exports = router;