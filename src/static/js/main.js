function toggleMenu() {
	var menu = document.getElementById("menu");
	var button = document.getElementById("menu-button");

	if (menu.classList.contains("active")) {
		menu.classList.remove("active");
		setTimeout(function () {
			menu.classList.remove("show");
		}, 300);
		button.classList.remove("is-active");
	} else {
		menu.classList.add("show");
		setTimeout(function () {
			menu.classList.add("active");
		}, 10);
		button.classList.add("is-active");
	}
}

function toggleTopBarMenu() {
	var menu = document.getElementById("topbar");
	var button = document.getElementById("menu-button");

	if (menu.classList.contains("active")) {
		menu.classList.remove("active");
		button.classList.remove("is-active");
	} else {
		menu.classList.add("active");
		button.classList.add("is-active");
	}
}