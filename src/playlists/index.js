const express = require('express');
const router = express.Router();
const verify = require('../token').verify;
const requester = require('../requester');
const getAccount = require('../accounts/get');
const errors = require('../errors');
const render = require('../render');
const redirect = require('../redirect');
const fetchMessage = require('../messages').createMessage;

router.use(function(req, res, next) {
	verify(req, res, next);
});

router.get('/', function(req, res) {
	requester('/playlists' + (req.query.owner ? "?owner=" + req.query.owner : ''), 'GET', true, req.cookies["Authorization"])
	.then(function(body) {
		if (!body.data || !body.data.playlists) {
			throw new errors.InvalidRequest(body.message || "Couldn't fetch playlists.");
		} else if (req.query.owner) {
			getAccount(req.query.owner, req.cookies["Authorization"])
			.then(function(account) {
				render.playlists(req, res, body.data.playlists, req.cookies["Authorization"], account, fetchMessage(req));
			})
			.catch(function(err) {
				redirect(res, 'login', err);
			});
		} else {
			render.playlists(req, res, body.data.playlists, req.cookies["Authorization"], undefined, fetchMessage(req));
		}
	}).catch(function(err) {
		redirect(res, 'login', err);
	});
});

router.post('/', function(req, res) {
	requester('/playlists', 'POST', req.body, req.cookies["Authorization"])
	.then(function(body) {
		if (!body.data || !body.data.id) {
			throw new errors.InvalidRequest(body.message || "Couldn't create playlist.");
		}
		res.redirect('/playlists/' + body.data.id);
	})
	.catch(function(err) {
		redirect(res, 'playlists', err);
	});
});

router.get('/:id', function(req, res) {
	requester('/playlists/' + req.params.id, 'GET', true, req.cookies["Authorization"])
	.then(function(body) {
		if (!body.data || !body.data.playlist || body.data.songs === undefined) {
			throw new errors.InvalidRequest(body.message || "Couldn't fetch playlist.");
		}
		render.playlist(req, res, body.data.playlist, body.data.songs, req.cookies["Authorization"], fetchMessage(req));
	})
	.catch(function(err) {
		redirect(res, 'playlists', err);
	});
});

router.get('/:id/songs', function(req, res) {
	requester('/songs/' + (req.query.query ? "?query=" + req.query.query : ""), 'GET', true, req.cookies["Authorization"])
	.then(function(body) {
		if (!body.data || !body.data.songs || body.data.songs === undefined) {
			throw new errors.InvalidRequest(body.message || "Couldn't fetch songs.");
		}
		render.songs(req, res, req.params.id, body.data.songs, req.query.query, req.cookies["Authorization"], fetchMessage(req));
	})
	.catch(function(err) {
		console.log(err);
		redirect(res, 'playlists/' + Number(req.params.id), err);
	});
});

router.post('/modify', function(req, res) {
	requester('/playlists/' + Number(req.body.playlist), 'PUT', { name: req.body.name, private: req.body.private == "false" ? false : true }, req.cookies["Authorization"])
	.then(function(body) {
		if (!body.data || !body.data || !body.data.playlist) {
			throw new errors.InvalidRequest(body.message);
		}
		res.redirect('/playlists/' + req.body.playlist + "?message=" + body.message + "&type=" + body.type);
	})
	.catch(function(err) {
		redirect(res, 'playlists/' + Number(req.body.playlist), err);
	});
});

router.post('/delete', function(req, res) {
	requester('/playlists/' + Number(req.body.playlist), 'DELETE', true, req.cookies["Authorization"])
	.then(function(body) {
		if (body.type != "success") {
			throw new errors.InvalidRequest(body.message || "Couldn't delete playlist.");
		}
		res.redirect('/playlists?message=' + body.message + "&type=" + body.type);
	})
	.catch(function(err) {
		redirect(res, 'playlists/' + Number(req.body.playlist), err);
	});
});

module.exports = router;